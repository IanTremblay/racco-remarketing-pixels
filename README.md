# TO DO AS FAST AS POSSIBLE


# Email Encryption + Remarketing Pixel

Here are the installation details of the RACCO campaign's remaketing pixels.
Due to the nature of the campaign it will be important to install these pixels on both the websites and intranets of all schools and boards


## Table of Contents
----------------------------------------

  1. Encryptage instruction
  2. Facebook Pixel
  3. Adwords Pixel
  
  
## 1. Facebook Pixel
----------------------------------------


### Encryption instruction

Download QuickHash: http://quickhash-gui.org/downloads/

QuickHash makes this so simple to do.

  1. Choose SHA256, which is the encryption Google�s help files ask for.
  2. Copy all of the emails in your Excel file.
  3. Paste them into the big QuickHash window.
  4. Click the highlighted TEXT line-by-Line button
  5. It will ask you to save your file.
  6. Remove the column containing the non encryted email from the file.
  7. Save it.
  
Viola! 


## 2. Facebook Pixel
----------------------------------------


### instruction d'installation

You must install the pixel base code into the header code of every page of your website. Find 
the <head></head> tags in your webpage code, or locate the header template in your CMS or web 
platform and paste the pixel base code at the bottom of the header section, just above the 
</head> tag.

### Facebook Pixel

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1919760434955489'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1919760434955489&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->



## 3. Adwords Pixel
----------------------------------------


### instruction d'installation

Add this code to all your webpages, right before the </body> tag

### Adwords Pixel

<!-- Google Code for Remarketing Tag -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 837543278;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/837543278/?guid=ON&amp;script=0"/>
</div>
</noscript>


    
